# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-

from guardian import GuardState

request = 'COPY_AND_PASTE'
nominal = 'COPY_AND_PASTE'
class DO_NOTHING(GuardState):
    goto = True
    request = True
    def run(self):
        return True

class COPY_AND_PASTE(GuardState):
    request = True
    def run(self):
        log('running')
        ezca['LSC-X_COMM_ERR_OFFSET'] = ezca['IMC-F_OUT16']
        ezca['LSC-Y_COMM_ERR_OFFSET'] = ezca['IMC-F_OUT16']
        log('still running')
        return True

edges = [
     ('DO_NOTHING', 'COPY_AND_PASTE'),
     ('COPY_AND_PASTE','DO_NOTHING')
        ]
